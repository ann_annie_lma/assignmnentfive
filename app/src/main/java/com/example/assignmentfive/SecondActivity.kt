package com.example.assignmentfive

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import com.example.assignmentfive.SecondActivity.Constants.EMAIL
import com.example.assignmentfive.SecondActivity.Constants.PASSWORD
import com.example.assignmentfive.databinding.ActivityMainBinding
import com.example.assignmentfive.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private lateinit var binding:ActivitySecondBinding

    object Constants {
        const val EMAIL = "com.example.assignmentfive.email"
        const val PASSWORD = "com.example.assignmentfive.password"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val email = intent.getStringExtra(EMAIL)
        val password = intent.getStringExtra(PASSWORD)

        binding.txtEmail.text = email
        binding.txtPassword.text = password
    }
}