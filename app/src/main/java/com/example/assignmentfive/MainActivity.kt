package com.example.assignmentfive

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.assignmentfive.databinding.ActivityMainBinding
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            if(validateFields())
            {
                startNewActivity()
            }

        }
    }


    private fun validateFields():Boolean
    {
        if(binding.etEmail.text.toString().isEmpty() ||
            binding.etPassword.toString().isEmpty())
        {
            binding.txtMessage.setText(R.string.fill_all_fields)
            binding.txtMessage.setTextColor(Color.RED)
            return false
        }

        if(!emailValidator())
        {
            binding.txtMessage.setText(R.string.valid_email)
            binding.txtMessage.setTextColor(Color.RED)
            return false
        }


        return true
    }

    private fun emailValidator():Boolean
    {
        val emailRegex = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

        return emailRegex.matcher(binding.etEmail.text.toString()).matches()
    }

    private fun startNewActivity()
    {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra(SecondActivity.Constants.EMAIL,binding.etEmail.text.toString())
        intent.putExtra(SecondActivity.Constants.PASSWORD,binding.etPassword.text.toString())
        startActivity(intent)
    }
}